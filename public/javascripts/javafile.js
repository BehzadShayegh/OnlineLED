function send_request(url){
    const Http = new XMLHttpRequest();
    Http.open("Get", url);
    Http.send();
}

function Blue_on(){
    send_request('https://turnonmyled.herokuapp.com/order/blueon');
}
function Green_on(){
    send_request('https://turnonmyled.herokuapp.com/order/greenon');
}
function all_OFF(){
    send_request('https://turnonmyled.herokuapp.com/order/bothoff');
}