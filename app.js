const cool = require('cool-ascii-faces');
var express = require('express');
var app = express();
var path = require('path');
const PORT = process.env.PORT || 5000;

app.use(express.static('public'))

app.get('/', function(req, res){
    res.sendFile(path.join(__dirname+'/public/htmlfiles/index.html'));
});


var blue_on = false;
var green_on = false;


app.get('/order/blueon', function(req, res){
    blue_on = true;
});
app.get('/order/greenon', function(req, res){
    green_on = true;
});
app.get('/order/bothoff', function(req, res){
    blue_on = false;
    green_on = false;
});


app.get('/arduino_request', function(req, res){
    if(blue_on && green_on) res.send('both');
    if(blue_on) res.send('blue');
    if(green_on) res.send('green');
    else res.send('off');
})


app.listen(PORT);