#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

const int blue_led = 5;
const int green_led = 4;
bool blue = false;
bool green = false;

const char* ssid = "alireza";
//const char* password = "00000000";

void setup () {
  pinMode(blue_led, OUTPUT);
  pinMode(green_led, OUTPUT);
  
  Serial.begin(115200);
  WiFi.begin(ssid);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting.."); 
    }
}


void loop() { 
  if (WiFi.status() == WL_CONNECTED) { 
    //Check WiFi connection status 
    
    HTTPClient http; 
    //Declare an object of class HTTPClient 
    
    http.begin("http://turnonmyled.herokuapp.com/arduino_request"); 
    //Specify request destination 
    
    int httpCode = http.GET();
    //Send the request
    
    if (httpCode > 0) {
      //Check the returning code 
       
      String payload = http.getString();
      //Get the request response payload 
      
      Serial.println(payload);
      //Print the response payload

      if(payload == "blue"){
        blue = true;
      }
      else if(payload == "green"){
        green = true;
      }
      else if(payload == "both"){
        blue = true;
        green = true;
      }
      else if(payload == "off"){
        blue = false;
        green = false;
      }

      
    }
      
      http.end(); //Close connection 
  }
  else {
    Serial.println("there is no response!");
  }

   if(blue) digitalWrite(blue_led,HIGH);
   if(green) digitalWrite(green_led,HIGH);
   if(!blue) digitalWrite(blue_led,LOW);
   if(!green) digitalWrite(green_led,LOW);
   

    
    delay(1000);
}
